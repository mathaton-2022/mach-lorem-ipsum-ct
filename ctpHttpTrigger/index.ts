import {AzureFunction, Context, HttpRequest} from '@azure/functions';
import {CommercetoolsProductService} from '../clients/commercetools/client';


const httpTrigger: AzureFunction = async function(
    context: Context,
    req: HttpRequest):
Promise<void> {
  context.log('HTTP trigger function processed a request.');
  try {
    const ctpService = new CommercetoolsProductService(req.body.credentials);
    const body = req.method == 'GET' ?
    await ctpService.fetchAllProducts() :
    await ctpService.updateAllOnAllActions(req.body.actions);
    console.log(body);
    context.res = {body: body};
  } catch (err) {
    context.res = {
      status: 505,
      body: {
        message: err,
      },
    };
  }
};

export default httpTrigger;
