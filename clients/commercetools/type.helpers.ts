export const AvailableActions = {
  'addExternalImage': 'processUpdateExternalImageAction',
};

export type ProductActions = keyof typeof AvailableActions;

export type CtpConfig = {
    projectKey: string,
    apiHost: string,
    authHost: string,
    clientSecret: string,
    appId: string,
}

type AddNewImageActionData = {
    productId: string,
    img: string,
};

export type ActionDataType = AddNewImageActionData;

export type ActionType = {
    action: ProductActions,
    data: [ActionDataType]
};
