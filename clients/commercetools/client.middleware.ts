import {
  AuthMiddlewareOptions,
  HttpMiddlewareOptions} from '@commercetools/sdk-client-v2';
import fetch from 'node-fetch';


export const authMiddleware = (
    projectKey: string,
    appId: string,
    clientSecret: string): AuthMiddlewareOptions => {
  return {
    host: process.env.CommerceToolsAuthHost!,
    projectKey: projectKey,
    credentials: {
      clientId: appId!,
      clientSecret: clientSecret!,
    },
    scopes: [`manage_project:${projectKey}`],
    fetch: fetch as any,
  };
};

export const httpMiddleware : HttpMiddlewareOptions = {

  host: process.env.CommerceToolsApiHost!,
  fetch: fetch as any,

};

